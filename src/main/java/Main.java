import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        ArrayList<File> arr = new ArrayList<>();

        //написать метод который возвращает список всех файлов в папке, включая все вложенные папки
        File curFolder = new File(".");
        getInnerFiles(curFolder, arr);
        for (File f : arr
        ) {
            System.out.println(f.getAbsolutePath());
        }

        //записать весь перечень файлов, найденных предыдущим методом в текстовый файл в виде полных путей
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream("1.txt"))) {
            for (File f : arr) {
                outputStream.write(f.getAbsolutePath().getBytes(StandardCharsets.UTF_8));
                outputStream.write("\r\n".getBytes());
            }
            outputStream.flush();
        }

        // outputStream.close();

    }

    static List<File> getInnerFiles(File f, List<File> lst) {

        for (File ff : f.listFiles()) {
            if (ff.isFile()) {
                lst.add(ff);
            } else if (ff.getName().equals(".") || ff.getName().equals("..")) {
                continue;
            } else {
                getInnerFiles(ff, lst);
            }
        }
        return lst;
    }
}




